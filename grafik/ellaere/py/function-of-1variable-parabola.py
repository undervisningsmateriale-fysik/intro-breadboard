# -*- coding: utf-8 -*-
"""
@author: msm
"""

# import packages to ensure python3 compatibility
from __future__ import print_function
from __future__ import division
#from __future__ import unicode_literals

# import packages
import numpy as np
import matplotlib.pyplot as plt

# make graph
fig = plt.figure()    
#ax = fig.add_axes([0, 0, 0.85, 0.85], projection='3d')
ax = plt.subplot(111)
ax.grid(True)

x = np.arange(-3,3,0.05)

ax.plot(x,x**2+1, lw='3')

ax.set_xlabel('x', fontsize='17')
ax.set_ylabel('f(x)', fontsize='17')


# prepare the axes limits
ax.set_xlim((-3,3))
ax.set_ylim((0, 10))

# set "camera" view
#ax.elev = 20 # elevation
#ax.azim = -90 # azimuth angle
#ax.dist = 8 # distance

plt.show()